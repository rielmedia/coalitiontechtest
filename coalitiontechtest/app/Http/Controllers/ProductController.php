<?php

namespace App\Http\Controllers;

use STDClass;
use Validator;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\Controller;

class ProductController extends Controller
{

	public function Index(){
		$products = Product::all();
		if(count($products) > 0){
			Storage::put('products.json', json_encode($products));
		}
		
		$output = array('products' => Product::all(), 'total' => $this->getTotal(Product::all()));
		return view('products.index',$output);
	}

	public function postIndex(Request $request){
		if($request){
			//dd($request->all());
			$action = ($request->input('action')) ? $request->input('action') : false;
			switch($action){
				case "Add":
                    // Validate aquisition data
                    $validation = Validator::make($request->all(), Product::$rules);
                    if($validation->fails()){
                        return back()->withErrors($validation->messages());
                    }

					$product_name 	= $request->input('product_name');
					$product_qty 	= $request->input('product_qty');
					$product_price 	= str_replace(',','',$request->input('product_price'));

                    $exists = Product::where('product_name','=',$product_name)->where('product_price','=',$product_price)->first();
                    if($exists){
                		$exists->product_name		= $product_name;
                		$exists->product_qty		= $product_qty;
                		$exists->product_price		= $product_price;
                		$exists->total_value		= ($product_qty * $product_price);
                    	if(!$exists->save()){
                    		return back()->withErrors('Unable to update product.');
                    	}

                    }else{
                    	$create = Product::create([
                    		'product_name'		=> $product_name,
                    		'product_qty'		=> $product_qty,
                    		'product_price'		=> $product_price,
                    		'total_value'		=> ($product_qty * $product_price)
                    	]);
                    	if(!$create->save()){
                    		return back()->withErrors('Unable to create new product.');
                    	}
                    }
                    return back();
					break;
			}
		}
	}

	public function getTotal($products){
		$total = '';
		if($products){
			foreach($products as $product){
				$total = ($total + $product->total_value);
			}
		}
		return $total;
	}
}

?>