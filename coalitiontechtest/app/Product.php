<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public $table = 'products';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'product_name', 'product_qty', 'product_price', 'total_value'
    ];

    public static $rules = [
        'product_name'  =>  'required|regex:/^([a-zA-Z0-9\.\ \-])+$/i',
        'product_qty'   =>  'required|regex:/^([0-9])+$/',
        'product_price' =>  'required|regex:/^([0-9\,])+\.+([0-9])+$/',
    ];
}
