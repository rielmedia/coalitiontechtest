<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <meta name="description" content="@yield('meta-description')" />
        <meta name="keywords" content="@yield('meta-keywords')" />
        <meta name="csrf-token" content="{{ csrf_token() }}" />

        <title>@yield('title') | Products</title>

        <link href="{{ asset('css/app.css') }}" media="all" rel="stylesheet" type="text/css" />
        <link href="{{ asset('css/app.css.map') }}" media="all" rel="stylesheet" type="text/css" />
    </head>
    <body id="">
        <div id="page">

            <!-- NAVIGATION -->
		    <section id="header">
		        <div class="container">
    		        @include('includes.navigation')
                </div>
		    </section>