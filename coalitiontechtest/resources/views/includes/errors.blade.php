@if (session('status'))
<div class="alert alert-success">
	{{ session('status') }}
</div>
@endif

@if(Session::has('msg'))
<div class="alert alert-success" role="alert">
    <span class="success-icon glyphicon glyphicon-ok-sign" aria-hidden="true"></span>&nbsp;&nbsp;{{ Session::get('msg') }}<br />
</div>
@endif

@if(count($errors) > 0)
<div class="alert alert-danger" role="alert">
    <ul>
    @foreach($errors->all() as $error)
        <li class="error"><span class="danger-icon glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>&nbsp;&nbsp;{{ $error }}</li>
    @endforeach
    </ul>
</div>
@endif