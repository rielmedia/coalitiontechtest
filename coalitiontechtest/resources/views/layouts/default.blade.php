@include('includes.header')

    <section id="main">
        <div class="columns-one container">
            <div class="content">
                <h1>@yield('pagetitle')</h1>
                @yield('content')
            </div>
    </section>

@include('includes.footer')