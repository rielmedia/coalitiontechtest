@extends("layouts.default")

@section('title') Products @endsection
@section('pagetitle') Products @endsection
@section('meta-description')  @endsection
@section('meta-keywords')  @endsection

@section('content')
<div class="container">
	<div class="row">
		<div>
			@include('includes.errors')

			<form method="post" action="{{ url('/products') }}">
				<p>
					<label for="product_name">Product Name:</label><br>
					<input type="text" name="product_name" placeholder="Enter product name" value="{{ old('product_name') }}" />
				</p>
				<p>
					<label for="product_qty">Quantity in Stock:</label><br>
					<input type="text" name="product_qty" placeholder="5" value="{{ old('product_qty') }}" />
				</p>
				<p>
					<label for="product_price">Product Price:</label><br>
					<input type="text" name="product_price" placeholder="0.00" value="{{ old('product_price') }}" />
				</p>
				<p>
					{{ csrf_field() }}
					<input type="hidden" name="action" value="Add" />
					<input type="submit" name="submit" value="Add Product" />
				</p>
			</form>
		</div>
	</div>
</div>
<div class="container">
	<div class="row">
		<div id="productsTable">
			<table border-spacing="10" border-padding="10" class="col-xs-12">
				<tbody>
					<tr>
						<th class="text-left">Product name</th>
						<th class="text-center">Qty</th>
						<th class="text-right">Price</th>
						<th class="text-right">Date submitted</th>
						<th class="text-right">Total</th>
					</tr>
				</tbody>
				<tbody>
				@if($products)
					@foreach($products as $product)
						<tr>
							<td class="text-left">{{ ucwords($product->product_name) }}</td>
							<td class="text-center">{{ $product->product_qty }}</td>
							<td class="text-right">{{ number_format($product->product_price,2,",",",") }}</td>
							<td class="text-right">{{ date_format(date_create($product->created_at), 'M. d, Y') }}</td>
							<td class="text-right">{{ number_format($product->total_value,2,",",",") }}</td>
						</tr>
					@endforeach
				@endif
					<tr>
						<td class="text-left"></td>
						<td class="text-center"></td>
						<td class="text-right"></td>
						<td class="text-right"></td>
						<td class="text-right"><strong>{{ number_format($total,2,",",",") }}</strong></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>
@endsection